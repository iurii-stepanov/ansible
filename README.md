# ansible


## Установка Ansible

### Установка Ansible в Ubuntu 22.04

Официальная документация: 

[Installing Ansible - Ansible Documentation](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Проверяем, что python3 установлен

```bash
python3 --version

# Python 3.10.6
```

Проверяем, что установлен pip

```bash
python3 -m pip -V

# /usr/bin/python3: No module named pip
```

Устанавливаем pip

```bash
sudo apt update && sudo apt upgrade
sudo apt install python3-pip
```

Устанавливаем Ansible

```bash
python3 -m pip install --user ansible
```

Получаем сообщение

```bash
WARNING: The scripts ansible, ansible-config, ansible-connection, ansible-console, ansible-doc, ansible-galaxy, ansible-inventory, ansible-playbook, ansible-pull and ansible-vault are installed in '/home/uri/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
WARNING: The script ansible-community is installed in '/home/uri/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
```

Добавляем пути в $PATH

```bash
vim ~/.bashrc

# Добавить в конец файла строку:
export PATH="$PATH:/home/uri/.local/bin"

source ~/.bashrc
```

Проверяем установку

```bash
ansible --version
python3 -m pip show ansible
```

### Установка Ansible в Mac OS

Устанавливаем Ansible

```bash
pip3 install ansible
```

Проверяем установку

```bash
ansible --version
python3 -m pip show ansible
```

Добавляем пусть в $PATH

```bash
vim ~/.zshrc

# Добавляем строку
export PATH=/Users/istepanov/Library/Python/3.9/bin:$PATH

source ~/.zshrc
```

## Подключение к Linux-серверам

Создаем папку проекта, в ней файл inventory `hosts.txt` такого содержания:

```bash
[staging_servers]
ansible-1    ansible_host=178.154.225.52    ansible_user=uri    ansible_ssh_private_key=/Users/istepanov/.ssh/yc_id_rsa

[prod_servers]
ansible-2    ansible_host=51.250.79.163     ansible_user=uri    ansible_ssh_private_key=/Users/istepanov/.ssh/yc_id_rsa
```

Запускаем модуль ping для группы [staging_servers]

```bash
ansible -i hosts.txt staging_servers -m ping
```

Создаем конфиг `ansible.cfg`:

```bash
[defaults]
host_key_checking = false
inventory = ./hosts.txt
```

Теперь запускать можно без указания inventory файла

```bash
ansible all -m ping
```

## Группы и переменные в inventory файле

Например:

```bash
[staging_servers]
ansible-1 ansible_host=192.168.3.12

[staging_servers:vars]
ansible_user=uri
ansible_ssh_private_key_file=/Users/istepanov/.ssh/yc_id_rsa

[prod_servers]
ansible-2 ansible_host=192.168.3.10

[prod_servers:vars]
ansible_user=uri
ansible_ssh_private_key_file=/Users/istepanov/.ssh/yc_id_rsa

[all_servers:children]
staging_servers
prod_servers
```

## Ad-hoc  команды

`setup` - информация о сервере 

```bash
ansible staging_servers -m setup
```

`shell` - выполнение команды

```bash
ansible all -m shell -a "uptime"
ansible staging_servers -m shell -a "ls /etc"
```

`command` - выполнение команды без переменных среды (не работают переменные среды: $HOME, пайп: |, перенаправления: >, < и т.д.) 

```bash
ansible staging_servers -m command -a "ls /var"
```

`copy` - копирование файлов с заданием прав на файл и sudo (-b - become) 

ПРИМЕЧАНИЕ: при подключение ansible через ssh_key нужно разрешить команды sudo без пароля: [https://stackoverflow.com/questions/27374110/ansible-hosts-configuration-using-private-key-and-sudo-user](https://stackoverflow.com/questions/27374110/ansible-hosts-configuration-using-private-key-and-sudo-user)

```bash
# без sudo
ansible all -m copy -a "src=./hello.txt dest=/home/uri mode=777"

# с sudo 
ansible all -m copy -a "src=./hello.txt dest=/home mode=777" -b
```

`file` - работа с файлами и директориями

Удаление файла:

```bash
ansible all -m file -a "path=/home/hello.txt state=absent" -b
```

`get_url` - скачать файл из интернета

```bash
ansible all -m get_url -a "url=http://archive.ubuntu.com/ubuntu/pool/universe/m/mc/mc_4.8.24-2ubuntu1_amd64.deb dest=/home/uri"
```

`apt` - установка и удаление пакетов  в Ubuntu

```bash
# установка
ansible all -m apt -a "name=stress state=latest" -b

# удаление
ansible all -m apt -a "name=stress state=ansent" -b
```

`uri` - проверка доступности url/uri

```bash
# просто проверка URL
ansible all -m uri -a "url=http://ya.ru"

# с контентом
ansible all -m uri -a "url=http://ya.ru return_content=yes"
```

`service` - работа со службами

```bash
# установка Apache2
ansible all -m apt -a "name=apache2 state=latest" -b

# старт Apache2 и добавление в автозагрузку
ansible all -m service -a "name=apache2 state=started enabled=yes" -b

# удаление Apache2
ansible all -m apt -a "name=apache2 state=absent" -b
```

`-v, -vv, -vvv, -vvvv` - дебаг

```bash
ansible ansible-1 -m shell -a "uptime" -v
ansible ansible-1 -m shell -a "uptime" -vv
ansible ansible-1 -m shell -a "uptime" -vvv
ansible ansible-1 -m shell -a "uptime" -vvvv
```

## Перенос переменных в group_vars

В директории проекта создаем директорию `group_vars` , в ней файлы yaml по названиям групп с переменными

```bash
├── ansible.cfg
├── group_vars
│   ├── prod_servers.yaml
│   └── staging_servers.yaml
├── hosts.txt

```

Например:

```yaml
ansible_user: uri
ansible_ssh_private_key_file: /Users/istepanov/.ssh/yc_id_rsa
```

## Плэйбуки

### Ping

Создаем файл `playbook-1_test_connection.yaml`

```yaml
- name: Test Connection To My Servers
  hosts: all
  become: yes

  tasks:
  - name: Ping My Servers
    ping:
```

Запускаем playbook

```bash
ansible-playbook playbook-1_test_connection.yaml
```

### Install Apache2

Создаем файл `playbook-2_install_apache2.yaml`

```yaml
- name: Install, Start and Enable Apache2
  hosts: all
  become: yes

  tasks:
  - name: Install Apache2
    apt: name=apache2 state=latest

  - name: Start and Enable Apache2
    service: name=apache2 state=started enabled=yes
```

Запускаем playbook

```bash
ansible-playbook playbook-2_install_apache2.yaml
```

### Deploy WebSite

Пишем playbook.

Здесь вызывается handler `Restart Apache2`, если при копировании файл `./test_files/MyWebSite/index.html` был изменен.

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    src_file: ./test_files/MyWebSite/index.html
    dest_file: /var/www/html

  tasks:
  - name: Install Apache2
    apt: name=apache2 state=latest

  - name: Copy My Website to Server
    copy: src={{ src_file }} dest={{ dest_file }} mode=0555
    notify: Restart Apache2

  - name: Start and Enable Apache2
    service: name=apache2 state=started enabled=yes

  handlers:
  - name: Restart Apache2
    service: name=apache2 state=restarted
```

Удаляем Apache2

```bash
ansible all -m apt -a "name=apache2 state=absent" -b
```

Запускаем playbook

```bash
ansible-playbook playbook-3_deploy_website.yaml
```

Если изменить файл `./test_files/MyWebSite/index.html` и запустить playbook повторно, то Apache2 будет перезапущен благодаря handler.

## Работа с переменными: debug, set_facts, register

В playbooks можно использовать все переменные, которые выводятся при выполнении модуля `setup`

```bash
ansible all -m setup
```

### debug

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    message1: Hello
    message2: World
    secret: LIGJDVKDJYUHEfjdfhjLdfhguIHUEKJFHJ

  tasks:
  - name: Print Var
    debug:
      var: secret

  - name: Print Var In Message
    debug:
      msg: "Secret is >>> {{ secret }} <<<" 

  - name: Print Var In Message From Inventory File
    debug:
      msg: "Owner Of This Server is >>> {{ owner }} <<<"
```

### set_facts

Можно писать таски без наваний

```yaml
- name: Playbook With Variables
  hosts: all
  become: yes

  vars:
    message1: Hello
    message2: World

  tasks:
##  - name: Set Facts
  - set_fact: full_message="{{ message1 }} {{ message2 }} from {{ owner }}"

##  - name: Hello From Server
  - debug:
      var: full_message
```

### register

register нужен для сохранения результатов выполнения модулей, которые не выводят результат, например `shell`

```yaml
- name: Playbook With Variables
  hosts: all
  become: yes

  tasks:
  - shell: uptime
    register: results

  - debug:
      var: results

```

## **Блоки и Условия – Block-When**

### when

Переделанный плэйбук установки Apache2

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    src_file: ./test_files/MyWebSite/index.html
    dest_file: /var/www/html

  tasks:
  - name: Check and Print OS Family
    debug:
      var: ansible_os_family

  - name: Install Apache2 To Debian
    apt: name=apache2 state=latest
    when: ansible_os_family == "Debian"

  - name: Install Apache2 To RedHat
    yum: name=httpd state=latest
    when: ansible_os_family == "RedHat"

  - name: Copy My Website to Server
    copy: src={{ src_file }} dest={{ dest_file }} mode=0555

  - name: Start and Enable Apache2 on Debian
    service: name=apache2 state=started enabled=yes
    when: ansible_os_family == "Debian"

  - name: Start and Enable Apache2 on RedHat
    service: name=httpd state=started enabled=yes
    when: ansible_os_family == "RedHat"
```

То есть при помощи `when` проверяется семейство ОС и пакет устанавливается по-разному, например здесь:

```yaml
  - name: Install Apache2 To Debian
    apt: name=apache2 state=latest
    when: ansible_os_family == "Debian"

  - name: Install Apache2 To RedHat
    yum: name=httpd state=latest
    when: ansible_os_family == "RedHat"
```

### block

Можно использовать условия только один раз внутри блока, например так:

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    src_file: ./test_files/MyWebSite/index.html
    dest_file: /var/www/html

  tasks:
  - name: Check and Print OS Family
    debug:
      var: ansible_os_family

  - block: # === BLOCK FOR DEBIAN ===
    - name: Install Apache2 To Debian
      apt: name=apache2 state=latest
    
    - name: Copy My Website to Server
      copy: src={{ src_file }} dest={{ dest_file }} mode=0555
      notify: Restart Apache2 on Debian
    
    - name: Start and Enable Apache2 on Debian
      service: name=apache2 state=started enabled=yes
    
    when: ansible_os_family == "Debian"
      
    
  - block: # === BLOCK FOR REDHAT ===
    - name: Install Apache2 To RedHat
      yum: name=httpd state=latest
  
    - name: Copy My Website to Server
      copy: src={{ src_file }} dest={{ dest_file }} mode=0555
      notify: Restart Apache2 on CentOS
  
    - name: Start and Enable Apache2 on RedHat
      service: name=httpd state=started enabled=yes
    
    when: ansible_os_family == "RedHat"

  handlers:
  - name: Restart Apache2 on Debian
    service: name=apache2 state=restarted

  - name: Restart Apache2 on CentOS
    service: name=httpd state=restarted
```

## Циклы

### loop

```yaml
- name: Learning Loops
  hosts: all
  become: yes

  tasks:
  - name: Hello To All from Loop
    debug: msg="Hello from {{ item }}"
    loop:
      - User_1
      - User_2
      - User_3
      - User_4

- name: Install Packages
    apt: name={{ item }} state=latest
    loop:
      - mc
      - tree
      - python
      - vim
```

### until

```yaml
- name: Learning Loops
  hosts: all
  become: yes

  tasks:
  - name: Loop Until Expression
    shell: echo -n "XYZ" >> loopfile.txt && cat loopfile.txt
    register: output  # регистрируем переменную, в которой результаты выполнения предыдущей команды
    delay: 2          # задержка между итерациями
    retries: 10.      # максимальное число итераций
    until: output.stdout.find("XYZXYZXYZ") == false   # условие выхода из цикла

  - name: Print Result Of Loop Until Expression
    debug: 
      var: output.stdout
```

### копирование списка файлов

Переделаем плэйбук разворачивания вэбсайта и скопируем несколько файлов двумя способами:

```yaml
- name: Copy My Website To Debian
  copy: src={{ src_folder }}{{ item }} dest={{ dest_folder }} mode=0555
  loop:
    - index.html
    - 1.txt
    - 2.txt
    - 3.txt

- name: Copy My Website To Debian
  copy: src={{ item }} dest={{ dest_folder }} mode=0555
  with_fileglob: "{{ src_folder }}/*.*"
```

Полный плэйбук:

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    src_folder: ./test_files/MyWebSite/
    dest_folder: /var/www/html

  tasks:
  - name: Check and Print OS Family
    debug:
      var: ansible_os_family

    # === BLOCK FOR DEBIAN ===
  - block:
    - name: Install Apache2 To Debian
      apt: name=apache2 state=latest
    - name: Start and Enable Apache2 on Debian
      service: name=apache2 state=started enabled=yes  
    when: ansible_os_family == "Debian"

    # === BLOCK FOR REDHAT ===        
  - block:
    - name: Install Apache2 To RedHat
      yum: name=httpd state=latest
    - name: Start and Enable Apache2 on RedHat
      service: name=httpd state=started enabled=yes
    when: ansible_os_family == "RedHat"

  - name: Copy My Website To Debian
    # copy: src={{ src_folder }}{{ item }} dest={{ dest_folder }} mode=0555
    # loop:
    #   - index.html
    #   - 1.txt
    #   - 2.txt
    #   - 3.txt
    copy: src={{ item }} dest={{ dest_folder }} mode=0555
    with_fileglob: "{{ src_folder }}/*.*"

    notify: 
      - Restart Apache2 on Debian
      - Restart Apache2 on CentOS

  handlers:
  - name: Restart Apache2 on Debian
    service: name=apache2 state=restarted
    when: ansible_os_family == "Debian"

  - name: Restart Apache2 on CentOS
    service: name=httpd state=restarted
    when: ansible_os_family == "RedHat"
```

## Jinja2 Templates

Создаем файл шаблона `index.j2` и указываем в нем переменные

```html
<h2>Hello from Apache2 inslalled by Ansible!</h2><br><br>

Owner of this server is: {{ owner }}<br>
IP address of this server is {{ ansible_default_ipv4.address }}<br>
Hostname is: {{ ansible_hostname }}<br>
OS family is: {{ ansible_os_family }}
```

В плэйбуке вместо копирования index.html вызываем команду `template`

```yaml
- name: Genetate index.html From Temlate
    template: src={{ src_folder }}/index.j2 dest={{ dest_folder }}/index.html mode=0555
```

Полный плэйбук:

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  vars:
    src_folder: ./test_files/MyWebSite/
    dest_folder: /var/www/html

  tasks:
  - name: Check and Print OS Family
    debug:
      var: ansible_os_family

    # === BLOCK FOR DEBIAN ===
  - block:
    - name: Install Apache2 To Debian
      apt: name=apache2 state=latest
    - name: Start and Enable Apache2 on Debian
      service: name=apache2 state=started enabled=yes  
    when: ansible_os_family == "Debian"

    # === BLOCK FOR REDHAT ===        
  - block:
    - name: Install Apache2 To RedHat
      yum: name=httpd state=latest
    - name: Start and Enable Apache2 on RedHat
      service: name=httpd state=started enabled=yes
    when: ansible_os_family == "RedHat"

  - name: Genetate index.html From Temlate
    template: src={{ src_folder }}/index.j2 dest={{ dest_folder }}/index.html mode=0555

  - name: Copy TXT Files 
    # copy: src={{ src_folder }}{{ item }} dest={{ dest_folder }} mode=0555
    # loop:
    #   - 1.txt
    #   - 2.txt
    #   - 3.txt
    copy: src={{ item }} dest={{ dest_folder }} mode=0555
    with_fileglob: "{{ src_folder }}/*.txt"

    notify: 
      - Restart Apache2 on Debian
      - Restart Apache2 on CentOS

  handlers:
  - name: Restart Apache2 on Debian
    service: name=apache2 state=restarted
    when: ansible_os_family == "Debian"

  - name: Restart Apache2 on CentOS
    service: name=httpd state=restarted
    when: ansible_os_family == "RedHat"
```

## Roles

Создаем директорию `roles` в проекте, в ней выполняем команду:

```yaml
ansible-galaxy init deploy-apache-web
```

По файлам и папкам растаскиваем весь проект.

Плэйбук выглядит так:

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: all
  become: yes

  roles:
    # - deploy-apache-web
    - { role: deploy-apache-web, when: ansible_system == 'Linux' }
```

`roles/deploy-apache-web/defaults/mail.yml`

```yaml
---
# defaults file for deploy-apache-web

dest_folder: /var/www/html
```

`roles/deploy-apache-web/handlers/main.yml`

```yaml
---
# handlers file for deploy-apache-web

- name: Restart Apache2 on Debian
  service: name=apache2 state=restarted
  when: ansible_os_family == "Debian"

- name: Restart Apache2 on CentOS
  service: name=httpd state=restarted
  when: ansible_os_family == "RedHat"
```

`roles/deploy-apache-web/tasks/main.yml`

```yaml
---
# tasks file for deploy-apache-web

- name: Check and Print OS Family
  debug:
    var: ansible_os_family

  # === BLOCK FOR DEBIAN ===
- block:
  - name: Install Apache2 To Debian
    apt: name=apache2 state=latest
  - name: Start and Enable Apache2 on Debian
    service: name=apache2 state=started enabled=yes  
  when: ansible_os_family == "Debian"

  # === BLOCK FOR REDHAT ===        
- block:
  - name: Install Apache2 To RedHat
    yum: name=httpd state=latest
  - name: Start and Enable Apache2 on RedHat
    service: name=httpd state=started enabled=yes
  when: ansible_os_family == "RedHat"

- name: Genetate index.html From Temlate
  template: src=index.j2 dest={{ dest_folder }}/index.html mode=0555
  notify: 
    - Restart Apache2 on Debian
    - Restart Apache2 on CentOS

- name: Copy TXT Files 
  copy: src={{ item }} dest={{ dest_folder }} mode=0555
  loop:
    - 1.txt
    - 2.txt
    - 3.txt
  # copy: src={{ item }} dest={{ dest_folder }} mode=0555
  # with_fileglob: "/*.txt"
  notify: 
    - Restart Apache2 on Debian
    - Restart Apache2 on CentOS
```

Структура роли:

```bash
.
└── deploy-apache-web
    ├── README.md
    ├── defaults
    │   └── main.yml
    ├── files
    │   ├── 1.txt
    │   ├── 2.txt
    │   └── 3.txt
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── tasks
    │   └── main.yml
    ├── templates
    │   └── index.j2
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
```

## Внешние переменные (extra_vars)

В плэйбуке введем переменную MY_HOSTS

```yaml
- name: Install Apache2 And Upload My WebSite
  hosts: "{{ MY_HOSTS }}"
  become: yes

  roles:
    - { role: deploy-apache-web, when: ansible_system == 'Linux' }
```

При запуске плэйбука указываем переменную

```yaml
ansible-playbook playbook-13_extra_vars.yaml --extra-vars "MY_HOSTS=staging_servers"
# или короче:
ansible-playbook playbook-13_extra_vars.yaml -e "MY_HOSTS=staging_servers"
```

Можно также переопределять уже существующие переменные, например, в инвентори файле есть переменная `owner`

```yaml
[staging_servers]
ansible-1 ansible_host=192.168.3.12      owner=user-1

[prod_servers]
ansible-2 ansible_host=192.168.3.10      owner=user-2

[staging_and_prod_servers:children]
staging_servers
prod_servers
```

но ее можно изменить

```yaml
ansible-playbook playbook-13_extra_vars.yaml --extra-vars "MY_HOSTS=staging_servers owner=USER_XXX"
```

### import_tasks и include_tasks

Отличия в том, что `import` подставляет контент до выполнения плэйбука, а `include` во время выполнения.

 Плэйбук:

```yaml
- name: Playbook With Includes And Imports
  hosts: all
  become: yes

  vars:
    demo_text: "Hello from include and import playbook"

  tasks:
  - name: Ping Servers
    ping:

  - name: Create folder_1 and folder_2
    import_tasks: playbook-14_include_create_folders.yaml
  
  - name: Copy file_1.txt And file_2.txt
    include_tasks: playbook-14_include_copy_files.yaml
```

`playbook-14_include_create_folders.yaml`

```yaml
- name: Create folder_1
  file:
    path: /home/uri/folder_1
    state: directory
    mode: 0755

- name: Create folder_2
  file:
    path: /home/uri/folder_2
    state: directory
    mode: 0755
```

`playbook-14_include_copy_files.yaml`

```yaml
- name: Copy file_1.txt
  copy:
    dest: /home/uri/folder_1/file_1.txt
    mode: 0755
    content: |
      Hello World!
      This is file_1.txt
      This is text from variable: {{ demo_text }}

- name: Copy file_2.txt
  copy:
    dest: /home/uri/folder_2/file_2.txt
    mode: 0755
    content: |
      Hello World!
      This is file_2.txt
      This is text from variable: {{ demo_text }}
```

## delegate_to

Не выполнять на одном из серверов таск, а делегировать другому, например Ansible Master (127.0.0.1)

Например, делегирование на Ansible Master

```yaml
  - name: Delegate to Ansible Master Server (MacBook)
    shell: echo This server {{ inventory_hostname }}:{{ ansible_default_ipv4.address }} was delegating from another server {{ ansible_nodename }}:{{ ansible_default_ipv4.address }} >> /Users/istepanov/Neoflex/Learning_Project/ansible/test_files/DELEGATING.txt
    delegate_to: 127.0.0.1
```

Или на другой сервер по его имени из инвентори файла

```yaml
  - name: Copy file_DELEGATE_1.txt
    copy:
      dest: /home/uri/folder_1/file_DELEGATE_1.txt
      mode: 0644
      content: |
        Hello World!
        This is text from variable: {{ demo_text }}
    delegate_to: ansible-1
```

Полный плэйбук:

```yaml
- name: Playbook With delegate_to
  hosts: all
  become: no

  vars:
    demo_text: "Hello from delegate_to Playbook"

  tasks:
  - name: Ping Servers
    ping:

  - name: Delegate to Ansible Master Server (MacBook)
    shell: echo This server {{ inventory_hostname }}:{{ ansible_default_ipv4.address }} was delegating from another server {{ ansible_nodename }}:{{ ansible_default_ipv4.address }} >> /Users/istepanov/Neoflex/Learning_Project/ansible/test_files/DELEGATING.txt
    delegate_to: 127.0.0.1
  

  - name: Copy file_DELEGATE_1.txt
    copy:
      dest: /home/uri/folder_1/file_DELEGATE_1.txt
      mode: 0644
      content: |
        Hello World!
        This is text from variable: {{ demo_text }}
    delegate_to: ansible-1

  - name: Copy file_DELEGATE_2.txt
    copy:
      dest: /home/uri/folder_2/file_DELEGATE_2.txt
      mode: 0644
      content: |
        Hello World!
        This is text from variable: {{ demo_text }}
```

## delegate_to: перезагрузка сервера

Для перезагрузки серверов нужно ждать старта на каком-нибудь другом сервере, например на Ansible Master (я не стал ждать на локалхосте, т.к. не хотел вводить root пароль для него)

```yaml
- name: Playbook With delegate_to
  hosts: ansible-1                 # выполняем только для ansible-1
  become: yes

  tasks:
  - name: Ping Servers First Time
    ping:

  - name: Reboot Servers
    shell: sleep 3 && reboot now   # перед перезагрузкой нужно немного подождать
    async: 1
    poll: 0                         # сразу отключаться от ssh

  - name: Wait Till Servers Will Come Up Online
    wait_for: 
      host: "{{ inventory_hostname }}"
      state: started
      delay: 5                      # через сколько начинать ждать (чтобы успел начать перезагружаться)
      timeout: 40                   # максимальное время ожидания
    delegate_to: ansible-2          # где ждать, т.к. на перезагружаемых серверах этого делать нельзя, ждем на ansible-1

  - name: Ping Servers Second Time
    ping:
```

## run_once

Запускает таск только на одном сервере (первом из инвентори файла), даже если плэйбук выполняется для нескольких серверов. То есть первый пинг на одном, второй - на всех.

```yaml
- name: Playbook With run_once
  hosts: all
  become: yes

  tasks:
  - name: Ping Servers First Time
    ping:
    run_once: true

  - name: Ping Servers Second Time
    ping:
```

## Перехват и контроль ошибок

### ignore_errors

Чтобы после неудачного выполнения таска остальные таски продолжали выполняться, можно добавить `ignore_errors`

```yaml
- name: Playbook With ignore_errors
  hosts: all
  become: yes

  tasks:
  - name: Task 1 - WITH ERROR
    apt: name=treeeeeee state=latest
    ignore_errors: true

  - name: Task 2
    shell: echo "Hello World!"
```

### failed_when: перехват ошибки в зависимости от содержимого переменной

```yaml
- name: Playbook With ignore_errors
  hosts: all
  become: yes

  tasks:
  - name: Task 1
    shell: echo "Hello World!"
    register: results
    failed_when: "'World' in results.stdout"

  - debug:
      var: results

  - name: Task 2
    shell: echo "Hello to All!"
```

### failed_when: перехват ошибки в зависимости от return code (rc)

```yaml
- name: Playbook With ignore_errors
  hosts: all
  become: yes

  tasks:
  - name: Task 1
    shell: echo "Hello World!"
    register: results
    failed_when: results.rc == 0    # здесь мы считаем, что rc=0 - это неудача

  - debug:
      var: results

  - name: Task 2
    shell: echo "Hello to All!"
```

### any_errors_fatal: прекратить выполнение плэйбука на всех серверах, если на одном возникла ошибка

Допустим, файл `/home/uri/file1.txt` есть на одном сервере, но нет на втором. Чтобы прекратить выполнение остальных таском на сервере, где ошибки не было, добавляется `any_errors_fatal` в начало плэйбука

```yaml
- name: Playbook With ignore_errors
  hosts: all
  become: yes
  any_errors_fatal: true

  tasks:
  - name: Task 1
    shell: cat /home/uri/file1.txt

  - name: Task 2
    ping:
```

## Ansible Vault

### Полезные команды

Создать зашифрованный файл (пароль, чтобы не забыть: 000000)

```yaml
ansible-vault create ansible-vault-mysecret.txt
```

Посмотреть содержимое файла

```yaml
ansible-vault view ansible-vault-mysecret.txt
```

Редактирование

```yaml
ansible-vault edit ansible-vault-mysecret.txt
```

Поменять пароль

```yaml
ansible-vault rekey ansible-vault-mysecret.txt
```

Зашифровать уже существующий файл

```yaml
ansible-vault encrypt ansible-vault-mysecret_2.txt
```

Расшифровать

```yaml
ansible-vault decrypt ansible-vault-mysecret_2.txt
```

Запустить зашифрованный плэйбук с вводом пароля

```yaml
ansible-playbook playbook.yaml --ask-vault-pass
```

Запустить зашифрованный плэйбук с указанием файла с паролем

```yaml
ansible-playbook playbook.yaml --vault-password-file mypass.txt
```

### Хранение секретов

Допустим есть плэйбук с паролем в открытом виде

```yaml
- name: Playbook With Ansible-Vault
  hosts: all
  become: yes

  vars:
    admin_password: Passw0rD

  tasks:
  - name: Install Package tree
    apt: name=tree state=latest

  - name: Create Config File
    copy:
      dest: "/home/uri/test_config.conf"
      content: |
        port: 33543
        user: admin
        log: 7days
        password: {{ admin_password }}
```

Шифруем строку

```yaml
ansible-vault encrypt_string

# пишем содержимое и для выхода нажимаем дважду CTRL-D
```

```yaml
!vault |
          $ANSIBLE_VAULT;1.1;AES256
          61316661616234366437626537666637613031626436396338656164653862656536366533383835
          6439656231653863626662363236303037356332373633350a376631376565306662363435376337
          37636163303665316635306165653331386164333336316562323532336232343566303532306135
          6364316264653964380a663030333239663030333237653837636630346332666637373033396461
          6431
```

Меняем в плэйбуке пароль на шифр

```yaml
- name: Playbook With Ansible-Vault
  hosts: all
  become: yes

  vars:
    admin_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          61316661616234366437626537666637613031626436396338656164653862656536366533383835
          6439656231653863626662363236303037356332373633350a376631376565306662363435376337
          37636163303665316635306165653331386164333336316562323532336232343566303532306135
          6364316264653964380a663030333239663030333237653837636630346332666637373033396461
          6431

  tasks:
  - name: Install Package tree
    apt: name=tree state=latest

  - name: Create Config File
    copy:
      dest: "/home/uri/test_config.conf"
      content: |
        port: 33543
        user: admin
        log: 7days
        password: {{ admin_password }}
```

Запускаем плэйбук

```yaml
ansible-playbook playbook.yaml --ask-vault-pass
```

### Шифрование через пайп

```yaml
echo -n "Password123" | ansible-vault encrypt_string
```

```yaml
 
```