[staging_servers]
ansible-1 ansible_host=192.168.3.12      owner=user-1

[prod_servers]
ansible-2 ansible_host=192.168.3.10      owner=user-2

# [fedora_servers]
# ansible-3 ansible_host=192.168.3.11      owner=user-3


[staging_and_prod_servers:children]
staging_servers
prod_servers
# fedora_servers